import scrapy
from ..items import WebcrawlerItem


class MySpider(scrapy.spiders.Spider):
    name = "MySpider"
    start_urls = {
        'https://www.w3schools.com/'
    }

    def parse(self, response):

        items = WebcrawlerItem()

        title = response.css('title ::text').extract()
        description = response.xpath("//meta[@name='Description']/@content")[0].extract()
        links = response.xpath("//a/@href").extract()

        items['title'] = title
        items['description'] = description
        items['links'] = links

        yield items


    def start_requests(self):
        url = "https://www.w3schools.com/"
        # Set the headers here. The important part is "application/json"
        headers = {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.84 Safari/537.36'
        }
        yield scrapy.http.Request(url, headers=headers)


